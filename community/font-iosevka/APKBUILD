# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=28.0.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/IosevkaCurlySlab-*.ttc
}

sha512sums="
9275a522c2f9e22c08f6c2b1ad680538e0102f33470871b1c0ea7297a66bd1dd5528db64fa8b5eace4033d08ad17b835b2df939af212b79c95435d6b917a5193  PkgTTC-Iosevka-28.0.1.zip
7a8d232967b6c15f20ff2000d860d14e58453d1ae974b4c3ba7e478553e0e86c6e163f356778a82210b8d5ea9eea08e43c7d36fc6cf7b0b0badc544472507ff0  PkgTTC-IosevkaAile-28.0.1.zip
4574df0fd0f99e30c31279817c669a354d344f08bcd0cd76d58cd3ae9bfd41b24cdc5b22f40f84604d47d5eb01d2dc207f65001c81bccec61308ef622c6fc289  PkgTTC-IosevkaSlab-28.0.1.zip
6beba36cfd9d23521ef4689ec66566dbf885b75da1b6c0e8276b7a26f2cdca45eafd35474f8d2b793db279d64558f13ae1f19bcc9efda0b4ec2d97c6fbafbda5  PkgTTC-IosevkaCurly-28.0.1.zip
aaf0ed336fcea326b8b1038e08c46f32721a4da07ee83100d2302497d9f189b2ffc4134aa31000d13c3475a26e4a30aa4284a3f934d7930045abae1943bf5747  PkgTTC-IosevkaCurlySlab-28.0.1.zip
"
