# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-chameleon
pkgver=4.4.2
pkgrel=0
pkgdesc="Fast Python HTML/XML Template Compiler"
url="https://chameleon.readthedocs.org"
arch="noarch"
license="BSD-3-Clause AND BSD-4-Clause AND Python-2.0 AND ZPL-2.1"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/malthe/chameleon/archive/$pkgver.tar.gz"
builddir="$srcdir"/chameleon-$pkgver

replaces="py-chameleon" # Backwards compatibility
provides="py-chameleon=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest \
		--ignore src/chameleon/tests/test_doctests.py \
		--ignore src/chameleon/tests/test_loader.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	rm -r "$pkgdir"/usr/lib/python3*/site-packages/chameleon/tests
}

sha512sums="
9255bbd3620c3f8c0f814c622544d5d4167cf03265be097095f271930fbadb09534f62d4b4332374b62a48fa9cef03a2bf2057239e9d4a99b17969d5698a0b79  py3-chameleon-4.4.2.tar.gz
"
